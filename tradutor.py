# -*- coding: utf-8 -*-

from le_token import AnalisadorLexico
import sys

class AnalisadorSintatico(object):
     
    def __init__(self, nome_arquivo):
        self.analisador = AnalisadorLexico()
        self.analisador.analisaArquivo(nome_arquivo)
        self.lista_erros = []
        self.blocos = []
        self.token_completo = {}
        self.mensagem_erro = "Erro: esperado %s. Encontrado: %s. Linha: %s Coluna: %s"
        self.avanca_token()

    def analise_sintatica(self):
        programa_codigo = ["",]
        if self.PROGRAMA(programa_codigo):
            portugol = open("portugol", "w")
            portugol.write(programa_codigo[0])
            print programa_codigo[0]
        else:
            print "Nao Ok"
            for item in self.lista_erros:
                print item

    def tk_atual(self):
        return self.analisador.token_atual['lexema']
    
    def tk_anterior(self):
        return self.analisador.token_anterior['lexema']

    def avanca_token(self):
        self.analisador.leToken()
        self.token_completo = self.analisador.token_atual

    def marca_token(self):
        self.analisador.marcarToken()

    def retroceder_token(self):
        self.analisador.retrocederToken()

    def PROGRAMA(self, programa_codigo):
        lista_declaracoes_codigo = ["",]
        if self.LISTA_DECLARACOES(lista_declaracoes_codigo):
            lista_funcoes_codigo = ["",]
            if self.LISTA_FUNCOES(lista_funcoes_codigo):
                if self.tk_atual() == "Fim de Arquivo":

                    programa_codigo[0] = "Algoritmo\nvar\n%s\n%s\ninicio\nmain()\nfimalgoritmo" % (lista_declaracoes_codigo[0], lista_funcoes_codigo[0])
                    programa_codigo[0] = self.tratar_printf_scanf(programa_codigo[0])
                    return True
                else:
                    self.logar_erro(["Fim de Arquivo"])
                    self.sair()
            return False
        return False

    def LISTA_DECLARACOES(self, lista_declaracoes_codigo):
        dec_var_fora_codigo = ["",]
        if self.DEC_VAR_FORA(dec_var_fora_codigo):
            if self.tk_atual() == ";":
                self.avanca_token()
                lista_declaracoes1_codigo = ["",]
                if self.LISTA_DECLARACOES(lista_declaracoes1_codigo):
                    lista_declaracoes_codigo[0] = dec_var_fora_codigo[0] + lista_declaracoes1_codigo[0]
                    return True
            else:
                self.logar_erro([";"])
                self.sair()
                return False
        return True

    def LISTA_FUNCOES(self, lista_funcoes_codigo):
        dec_fun_codigo = ["",]
        if self.DEC_FUN(dec_fun_codigo):
            lista_funcoes_codigo[0] = dec_fun_codigo[0]
            lista_declaracoes_codigo = ["",]
            if self.LISTA_DECLARACOES(lista_declaracoes_codigo):
                lista_funcoes1_codigo = ["",]
                if self.LISTA_FUNCOES(lista_funcoes1_codigo):
                    lista_funcoes_codigo[0] += lista_declaracoes_codigo[0] + lista_funcoes1_codigo[0]
                    return True
        else:
            return False
        return True

    def DEC_VAR_FORA(self, dec_var_fora_codigo):
        tipo_codigo = ["",]
        tipo, tokens_tipo = self.TIPO(tipo_codigo)
        if tipo:
            if self.tk_atual() == 'id':
                self.token_atual_completo()
                dec_var_fora_codigo[0] = self.token_completo['lexema_real']
                self.marca_token()
                self.avanca_token()
                resto_lids_codigo = ["",]
                if self.RESTO_LIDS(resto_lids_codigo):
                    if self.tk_atual() == ';':
                        dec_var_fora_codigo[0] = dec_var_fora_codigo[0] + resto_lids_codigo[0] + ":" + tipo_codigo[0] + "\n"
                        return True
                self.retroceder_token()
        for i in range(0, tokens_tipo):
            self.retroceder_token()
        return False

    def CMD_DEC_VAR(self, cmd_dec_var_declaracoes):
        tipo_codigo = ["",]
        tipo, tokens_tipo = self.TIPO(tipo_codigo)
        if tipo:
            if self.tk_atual() == 'id':
                id_val = self.token_completo["lexema_real"]
                self.marca_token()
                self.avanca_token()
                resto_lids_codigo = ["",]
                if self.RESTO_LIDS(resto_lids_codigo):
                    cmd_dec_var1_declaracoes = ["",]
                    if self.tk_atual() == ";":
                        self.avanca_token()
                        if self.CMD_DEC_VAR(cmd_dec_var1_declaracoes):
                            cmd_dec_var_declaracoes[0] = id_val + resto_lids_codigo[0] + ":" + tipo_codigo[0] + "\n" + cmd_dec_var1_declaracoes[0]
                            return True
                    else:
                        self.logar_erro([";"])
                        self.sair()
                self.retroceder_token()
        for i in range(0, tokens_tipo):
            self.retroceder_token()
        return True

    def TIPO(self, tipo_codigo):
        tokens_avancados = 0
        tipo_mod1, avan_mod1 = self.TIPO_MOD1()
        if tipo_mod1:
            tokens_avancados += avan_mod1
            tipo_mod2, avan_mod2 = self.TIPO_MOD2()
            if tipo_mod2:
                tokens_avancados += avan_mod2
                tipo_base_codigo = ["",]
                if self.TIPO_BASE(tipo_base_codigo):
                    tokens_avancados += 1
                    tipo_codigo[0] = tipo_base_codigo[0]
                    return True, tokens_avancados
        return False, tokens_avancados

    def TIPO_MOD1(self):
        tokens_avancados = 0
        if self.tk_atual() in ['unsigned','signed']:
            self.marca_token()
            self.avanca_token()
            tokens_avancados = 1
        return True, tokens_avancados

    def TIPO_MOD2(self):
        tokens_avancados = 0
        if self.tk_atual() in ['long','short']:
            self.marca_token()
            self.avanca_token()
            tokens_avancados = 1
        return True, tokens_avancados

    def TIPO_BASE(self, tipo_base_codigo):
        if self.tk_atual() in ['int','float','char','double','void']:
            if self.tk_atual() == "int":
                tipo_base_codigo[0] = "inteiro"
            elif self.tk_atual() == "float":
                tipo_base_codigo[0] = "real"
            elif self.tk_atual() == "char":
                tipo_base_codigo[0] = "literal"
            elif self.tk_atual() == "void":
                tipo_base_codigo[0] = ""
            self.marca_token()
            self.avanca_token()
            return True
        else:
            self.logar_erro(['int','float','char','double','void'])
            return False

    def RESTO_LIDS(self, resto_lids_codigo):
        if self.tk_atual() == ",":
            resto_lids_codigo[0] = ","
            self.marca_token()
            self.avanca_token()
            if self.tk_atual() == "id":
                self.token_atual_completo()
                resto_lids_codigo[0] += self.token_completo['lexema_real']
                self.marca_token()
                self.avanca_token()
                resto_lids1_codigo = ["",]
                if self.RESTO_LIDS(resto_lids1_codigo):
                    resto_lids_codigo[0] += resto_lids1_codigo[0]
                    return True
                else:
                    self.retroceder_token()
                    return False
            else:
                self.retroceder_token()
                return False
        elif self.tk_atual() == "=":
            self.marca_token()
            self.avanca_token()
            if self.FINAIS():
                if self.RESTO_LIDS():
                    return True
            else:
                self.retroceder_token()
                return False
        elif self.tk_atual() == "id":
            self.token_atual_completo()
            resto_lids_codigo[0] = self.token_completo['lexema_real']
            self.avanca_token()
            return True
        elif self.tk_atual() == ";":
            return True
        else:
            self.logar_erro([',', '=', 'id', ';'])
        return True

    def FINAIS(self, finais_codigo):
        if self.tk_atual() == "id":
            finais_codigo[0] = self.token_completo["lexema_real"]
            self.avanca_token()
            return True
        else:
            constante_codigo = ["",]
            if self.CONSTANTE(constante_codigo):
                finais_codigo[0] = constante_codigo[0]
                return True
        self.logar_erro(['id', 'cte_int', 'cte_float', 'cte_string'])
        return False

    def CONSTANTE(self, constante_codigo):
        if self.tk_atual() in ['cte_int','cte_float', 'cte_string']:
            constante_codigo[0] = self.token_completo["lexema_real"]
            self.avanca_token()
            return True
        else:
            self.logar_erro(['cte_int', 'cte_float', 'cte_string'])
            return False

    def DEC_FUN(self, dec_fun_codigo):
        tipo_opt_atr1 = ["",]
        tipo_opt_atr2 = ["",]
        tipo_opt_tipo = ["",]
        tipo_opt, tokens_avancados = self.TIPO_OPT(tipo_opt_atr1, tipo_opt_atr2, tipo_opt_tipo)
        if tipo_opt:
            dec_fun_codigo[0] = tipo_opt_atr1[0]
            if self.tk_atual() == 'id':
                dec_fun_codigo[0] += " " + self.token_completo['lexema_real'] 
                self.marca_token()
                self.avanca_token()
                if self.tk_atual() == '(':
                    dec_fun_codigo[0] += self.tk_atual()
                    self.marca_token()
                    self.avanca_token()
                    lista_args_codigo = ["",]
                    if self.LISTA_ARGS(lista_args_codigo):
                        dec_fun_codigo[0] += lista_args_codigo[0]
                        if self.tk_atual() == ')':  
                            dec_fun_codigo[0] += self.tk_atual() + tipo_opt_tipo[0] + "\n"
                            self.avanca_token()
                            bloco_cmd_fun_comandos = ["",]
                            bloco_cmd_fun_declaracoes = ["",]
                            if self.BLOCO_CMD_FUN(bloco_cmd_fun_comandos, bloco_cmd_fun_declaracoes):
                                #adiciona o codigo do bloco de comando
                                dec_fun_codigo[0] += bloco_cmd_fun_declaracoes[0] + bloco_cmd_fun_comandos[0] + "\n" + tipo_opt_atr2[0] + "\n\n"
                                return True
                        else:
                            self.logar_erro([")"])
                            self.sair()
                    self.sair()
                else:
                    self.logar_erro(["(", ";"])
                    self.sair()
            else:
                self.logar_erro(['id'])
            for i in range(0, tokens_avancados):
                self.retroceder_token()
        return False

    def TIPO_OPT(self, tipo_opt_atr1, tipo_opt_atr2, tipo_opt_tipo):
        tipo_codigo = ["",]
        tipo, tokens_avancados = self.TIPO(tipo_codigo)
        if tipo_codigo[0]:
            tipo_opt_atr1[0] = "funcao"
            tipo_opt_atr2[0] = "fimfuncao"
            tipo_opt_tipo[0] = " : " + tipo_codigo[0]
            return True, tokens_avancados
        tipo_opt_atr1[0] = "procedimento"
        tipo_opt_atr2[0] = "fimprocedimento"
        return True, tokens_avancados

    def LISTA_ARGS(self, lista_args_codigo):
        arg_codigo = ["",]
        if self.ARG(arg_codigo):
            resto_largs_codigo = ["",]
            if self.RESTO_LARGS(resto_largs_codigo):
                lista_args_codigo[0] = arg_codigo[0] + resto_largs_codigo[0]
                return True
        return False
 
    def ARG(self, arg_codigo = ["",]):
        tipo_codigo = ["",]
        tipo, tokens_avancados = self.TIPO(tipo_codigo)
        if tipo:
            if self.tk_atual() == "id":
                self.token_atual_completo()
                arg_codigo[0] = self.token_completo['lexema_real'] + " : " + tipo_codigo[0]
                self.avanca_token()
                return True
            else:
                self.logar_erro(['id'])
        return False

    def RESTO_LARGS(self, resto_largs_codigo = ["",]):
        if self.tk_atual() == ",":
            self.avanca_token()
            arg_codigo = ["",]
            if self.ARG(arg_codigo):
                resto_largs1_codigo = ["",]
                if self.RESTO_LARGS(resto_largs1_codigo):
                    resto_largs_codigo[0] = "; " + arg_codigo[0] + resto_largs1_codigo[0]
                    return True
                else:
                    return False
            else:
                return False
        return True

    def BLOCO_CMD_FUN(self, bloco_cmd_fun_comandos, bloco_cmd_fun_declaracoes):
        if self.tk_atual() == "{":
            self.marca_token()
            self.avanca_token()
            cmd_dec_var_codigo = ["",]
            if self.CMD_DEC_VAR(cmd_dec_var_codigo):
                bloco_cmd_fun_declaracoes[0] = "var\n" + cmd_dec_var_codigo[0]
                lista_cmd_codigo = ["",]
                if self.LISTA_CMD(lista_cmd_codigo):                
                    if self.tk_atual() == "}":
                        self.avanca_token()
                        bloco_cmd_fun_comandos[0] = "inicio\n" + lista_cmd_codigo[0]
                        return True
                    self.logar_erro(["}"])
                    self.sair()
                    return False
                self.sair()
        else:
            self.logar_erro(["{"])
        return False

    def CMD_BLOCO_CMD(self, cmd_bloco_cmd_codigo):
        if self.tk_atual() == "{":
            self.marca_token()
            self.avanca_token()
            self.blocos.append(1)
            lista_cmd_codigo = ["",]
            if self.LISTA_CMD(lista_cmd_codigo):
                if self.tk_atual() == "}":
                    if self.tk_anterior() in [";", "{", "}"]:
                        cmd_bloco_cmd_codigo[0] = lista_cmd_codigo[0]
                        return True
                else:
                    if self.tk_atual() == "Fim de Arquivo": 
                        if self.tk_anterior() in [";", "}"]:
                            cmd_bloco_cmd_codigo[0] = lista_cmd_codigo[0]
                            return True
                        self.logar_erro(["}"])
                        self.sair()
                self.logar_erro(["}"])
                self.sair()
                return False
            self.sair()
        else:
            self.logar_erro(["{"])
        return False

    def LISTA_CMD(self, lista_cmd_codigo):
        cmd_codigo = ["",]
        cmd, is_bloco_cmd = self.CMD(cmd_codigo) 
        if cmd:
            if self.tk_atual() in ["}", ";"]:
                if self.tk_atual() == ";": 
                    self.avanca_token()
                elif is_bloco_cmd and self.tk_anterior() in [";", "{", "}"]:
                    if self.blocos:
                        self.blocos.pop()
                        self.avanca_token()
                else:
                    self.logar_erro([";"])
                    return False
                lista_cmd1_codigo = ["",]
                if self.LISTA_CMD(lista_cmd1_codigo):
                    lista_cmd_codigo[0] = cmd_codigo[0] + "\n" + lista_cmd1_codigo[0]
                    return True
            if self.tk_anterior() in ["}", ";"]:
                lista_cmd1_codigo = ["",]
                if self.LISTA_CMD(lista_cmd1_codigo):
                    lista_cmd_codigo[0] = cmd_codigo[0] + "\n" + lista_cmd1_codigo[0]
                    return True
            self.logar_erro([";"])
            self.sair()
            return False
        return True

    def CMD(self, cmd_codigo):
        cmd_dec_var_declaracoes = ["",]
        cmd_while_codigo = ["",]
        cmd_dowhile_codigo = ["",]
        cmd_for_codigo = ["",]
        cmd_if_codigo = ["",]
        cmd_switch_codigo = ["",]
        cmd_case_codigo = ["",]
        cmd_default_codigo = ["",]
        cmd_dec_var_declaracoes = ["",]
        cmd_atribuicao_codigo = ["",]
        cmd_bloco_cmd_codigo = ["",]
        cmd_break_codigo = ["",]
        cmd_continue_codigo = ["",]
        cmd_return_codigo = ["",]
        if self.CMD_WHILE(cmd_while_codigo):
            cmd_codigo[0] = cmd_while_codigo[0]
            return True, self.tk_atual() == "}"
        elif self.CMD_DOWHILE(cmd_dowhile_codigo):
            cmd_codigo[0] = cmd_dowhile_codigo[0]
            return True, self.tk_atual() == "}"
        elif self.CMD_FOR(cmd_for_codigo):
            cmd_codigo[0] = cmd_for_codigo[0]
            return True, self.tk_atual() == "}"
        elif self.CMD_IF(cmd_if_codigo):
            cmd_codigo[0] = cmd_if_codigo[0]
            return True, self.tk_atual() == "}"
        elif self.CMD_SWITCH(cmd_switch_codigo):
            cmd_codigo[0] = cmd_switch_codigo[0]
            return True, self.tk_atual() == "}"
        elif self.CMD_ATRIBUICAO(cmd_atribuicao_codigo):
            cmd_codigo[0] = cmd_atribuicao_codigo[0]
            return True, self.tk_atual() == "}"
        elif self.CMD_BLOCO_CMD(cmd_bloco_cmd_codigo):
            cmd_codigo[0] = cmd_bloco_cmd_codigo[0]
            return True, self.tk_atual() == "}"
        elif self.CMD_BREAK(cmd_break_codigo):
            cmd_codigo[0] = cmd_break_codigo[0]
            return True, self.tk_atual() == "}"
        elif self.CMD_CONTINUE(cmd_continue_codigo):
            cmd_codigo[0] = cmd_continue_codigo[0]
            return True, self.tk_atual() == "}"
        elif self.CMD_RETURN(cmd_return_codigo):
            cmd_codigo[0] = cmd_return_codigo[0]
            return True, self.tk_atual() == "}"
        elif self.tk_atual() == ";":
            return True, self.tk_atual() == "}"
        else:
            if self.tk_atual() == "Fim de Arquivo":
                self.logar_erro(["}"])
            else:
                self.logar_erro([";"])
        return False, False

    def CMD_ATRIBUICAO(self, cmd_atribuicao_codigo):        
        if self.tk_atual() == "id":
            id_val = self.token_completo['lexema_real']
            self.avanca_token()
            cmd_atribuicaol_codigo = ["",]
            if self.CMD_ATRIBUICAOL(id_val, cmd_atribuicaol_codigo):
                cmd_atribuicao_codigo[0] = id_val + cmd_atribuicaol_codigo[0]
                return True
            cmd_atribuicao_codigo[0] = id_val
            return True
        else:
            op_pos_incremento_codigo = ["",]
            if self.OP_POS_INCREMENTO(op_pos_incremento_codigo, ""):
                self.marca_token()
                self.avanca_token()
                if self.tk_atual() == "id":
                    return True
                self.logar_erro(["id"])
                self.sair()
        return False

    def CMD_ATRIBUICAOL(self, id_val, cmd_atribuicaol_codigo):
        op_atribuicao_codigo = ["",]
        if self.OP_ATRIBUICAO(id_val, op_atribuicao_codigo):
            expressao_codigo = ["",]
            if self.EXPRESSAO(expressao_codigo):
                cmd_atribuicaol_codigo[0] = op_atribuicao_codigo[0] + expressao_codigo[0]
                return True
            return False
        else:
            op_pos_incremento_codigo = ["",]
            #id_val = ""
            #if self.tk_anterior() == "id":
                #id_val = self.analisador.token_anterior["lexema_real"]
            if self.OP_POS_INCREMENTO(op_pos_incremento_codigo, id_val):
                cmd_atribuicaol_codigo[0] = op_pos_incremento_codigo[0]
                return True
            elif self.tk_atual() == "(":
                self.marca_token()
                self.avanca_token()
                expressao_codigo = ["",]
                if self.EXPRESSAO(expressao_codigo):
                    if self.tk_atual() == ")":
                        self.avanca_token()
                        cmd_atribuicaol_codigo[0] = "(" + expressao_codigo[0] + ")"
                        return True
                    self.logar_erro([")"])
                    self.sair()
                self.sair()
            self.logar_erro(["("])
        return False
            
    def CMD_WHILE(self, cmd_while_codigo = ["",]):
        if self.tk_atual() == "while":
            self.marca_token()
            self.avanca_token()
            if self.tk_atual() == "(":
                self.marca_token()
                self.avanca_token()
                expressao_codigo = ["",]
                if self.EXPRESSAO(expressao_codigo):
                    if self.tk_atual() == ")":
                        self.marca_token()
                        self.avanca_token()                        
                        cmd_codigo = ["",]
                        if self.CMD(cmd_codigo)[0]:
                            cmd_while_codigo[0] = "enquanto " + expressao_codigo[0] + " faca\n" + cmd_codigo[0] + "\nfimenquanto"
                            return True
                        self.sair()
                    else:
                        self.logar_erro([")"])
                        self.sair()
                self.sair()
            else:
                self.logar_erro(["("])
                self.sair()
            self.retroceder_token()
        else: 
            self.logar_erro(["while"])
        return False
            
    def CMD_DOWHILE(self, cmd_dowhile_codigo):
        if self.tk_atual() == "do":
            self.avanca_token()
            cmd_codigo = ["",]
            if self.CMD(cmd_codigo)[0]:
                self.avanca_token()
                if self.tk_atual() == "while":
                    self.avanca_token()
                    if self.tk_atual() == "(":
                        self.avanca_token()
                        expressao_codigo = ["",]
                        if self.EXPRESSAO(expressao_codigo):
                            if self.tk_atual() == ")":
                                self.avanca_token()
                                if self.tk_atual() == ";":
                                    cmd_dowhile_codigo[0] = "repita\n" + cmd_codigo[0] + "\nate nao (" + expressao_codigo[0] + ")"
                                    return True
                                else:
                                    self.logar_erro([";"])
                                    self.sair()
                            else:
                                self.logar_erro([")"])
                                self.sair()
                        self.sair()
                    else:
                        self.logar_erro(["("])
                        self.sair()
                else:
                    self.logar_erro(["while"])
                    self.sair()
            self.sair()
        else:
            self.logar_erro(["do"])
        return False
 
    def CMD_FOR(self, cmd_for_codigo):
        if self.tk_atual() == "for":
            self.avanca_token()
            if self.tk_atual() == "(":
                self.avanca_token()
                lista_ini_codigo = ["",]
                if self.LISTA_INI(lista_ini_codigo):
                    if self.tk_atual() == ";":
                        self.avanca_token()
                        expressao_opt_codigo = ["",]
                        if self.EXPRESSAO_OPT(expressao_opt_codigo):
                            if self.tk_atual() == ";":
                                self.avanca_token()
                                lista_inc_codigo = ["",]
                                if self.LISTA_INC(lista_inc_codigo):
                                    if self.tk_atual() == ")":
                                        self.avanca_token()
                                        cmd_codigo = ["",]
                                        if self.CMD(cmd_codigo)[0]:
                                            cmd_for_codigo[0] = lista_ini_codigo[0] + "\nenquanto " + expressao_opt_codigo[0] + " faca\n" + cmd_codigo[0] + "\n" + lista_inc_codigo[0] + "\nfimenquanto"
                                            return True
                                        self.sair()
                                    else:
                                        self.logar_erro([")"])
                                        self.sair()
                                else:
                                    self.sair()
                            else:
                                self.logar_erro([";"])
                                self.sair()
                        self.sair()
                    else:
                        self.logar_erro([";"])
                        self.sair()
                else:
                    self.sair()
            else:
                self.logar_erro(["("])
                self.sair()
        else:
            self.logar_erro(["for"])
        return False
   
    def LISTA_INI(self, lista_ini_codigo):
        tipo_opt_atr1 = ["",]
        tipo_opt_atr2 = ["",]
        tipo_opt_tipo = ["",]
        tipo_opt, tokens_avancados = self.TIPO_OPT(tipo_opt_atr1, tipo_opt_atr2, tipo_opt_tipo)
        if tipo_opt:
            lista_ids_ini_codigo = ["",]
            if self.LISTA_IDS_INI(lista_ids_ini_codigo):
                lista_ini_codigo[0] = lista_ids_ini_codigo[0]
                return True
        #se estou com ; e porque achei uma LISTA_INI ou porque nao tinha nada
        if self.tk_atual() == ";":
            lista_ini_codigo[0] = ""
            return True
        else:
            self.logar_erro([";"])
            self.sair()
        return False

    def LISTA_IDS_INI(self, lista_ids_ini_codigo):
        if self.tk_atual() == "id":
            id_val = self.token_completo['lexema_real']
            self.avanca_token()
            resto_lids_ini_codigo = ["",]
            resto_lids_ini_valor = ["",]
            if self.RESTO_LIDS_INI(id_val, resto_lids_ini_codigo, resto_lids_ini_valor):
                #lista_ids_ini_codigo[0] = id_val + " <- " + 
                lista_ids_ini_codigo[0] = resto_lids_ini_codigo[0]
                return True
            self.sair()
        else:
            self.logar_erro(["id"])
        return False

    def RESTO_LIDS_INI(self, primeiro_id, resto_lids_ini_codigo, resto_lids_ini_valor):
        if self.tk_atual() == ",":
            self.avanca_token()
            if self.tk_atual() == "id":
                id_val = self.token_completo['lexema_real']
                self.marca_token()
                self.avanca_token()
                resto_lids_ini1_codigo = ["",]
                resto_lids_ini1_valor = ["",]
                if self.RESTO_LIDS_INI(id_val, resto_lids_ini1_codigo, resto_lids_ini1_valor):
                    if primeiro_id:
                        resto_lids_ini_codigo[0] = primeiro_id + " <- " + resto_lids_ini1_valor[0] + "\n" + resto_lids_ini1_codigo[0]
                    else:
                        resto_lids_ini_codigo[0] = resto_lids_ini1_codigo[0]
                    resto_lids_ini_valor[0] = resto_lids_ini1_valor[0]
                    return True
                self.retroceder_token()
            self.logar_erro(["id"])
            self.sair()
        elif self.tk_atual() == "=":
            self.avanca_token() 
            finais_codigo = ["",]
            if self.FINAIS(finais_codigo):
                resto_lids_ini_valor[0] = finais_codigo[0]
                if primeiro_id:
                    resto_lids_ini_codigo[0] = primeiro_id + " <- " + finais_codigo[0]
                resto_lids_ini1_codigo = ["",]
                resto_lids_ini1_valor = ["",]
                if self.RESTO_LIDS_INI("", resto_lids_ini1_codigo, resto_lids_ini1_valor):
                    resto_lids_ini_codigo[0] = resto_lids_ini_codigo[0] + "\n" + resto_lids_ini1_codigo[0]
                    return True
            self.sair()
        else:
            self.logar_erro([",", "="])
        return True

    def EXPRESSAO_OPT(self, expressao_opt_codigo):
        expressao_codigo = ["",]
        if self.EXPRESSAO(expressao_codigo):
            expressao_opt_codigo[0] = expressao_codigo[0]
            return True
        expressao_opt_codigo[0] = ""
        return True

    def LISTA_INC(self, lista_inc_codigo):
        expressao_codigo = ["",]
        if self.EXPRESSAO(expressao_codigo):
            lista_inc_codigo[0] = expressao_codigo[0].replace(", ", "\n")
            return True
        lista_inc_codigo[0] = ""
        return True

    def CMD_IF(self, cmd_if_codigo):
        if self.tk_atual() == "if":
            self.avanca_token()
            if self.tk_atual() == "(":
                self.marca_token()
                self.avanca_token()
                expressao_codigo = ["",]
                if self.EXPRESSAO(expressao_codigo):
                    if self.tk_atual() == ")":
                        self.avanca_token()
                        cmd_codigo = ["",]
                        if self.CMD(cmd_codigo)[0]:                            
                            self.marca_token()
                            self.avanca_token()
                            if self.tk_atual() == "else":
                                self.avanca_token()
                                cmd_codigo2 = ["",]
                                if self.CMD(cmd_codigo2)[0]:                                    
                                    cmd_if_codigo[0] = "se " + expressao_codigo[0] + " entao\n" + cmd_codigo[0] + "\nsenao\n" + cmd_codigo2[0] + "\nfimse"
                                    return True
                                self.sair()
                            else:
                                self.retroceder_token()
                                cmd_if_codigo[0] = "se " + expressao_codigo[0] + " entao\n" + cmd_codigo[0] + "\nfimse"
                                return True                                
                        else:
                            self.sair()
                    else:
                        self.logar_erro([")"])
                        self.sair()
                else:
                    self.sair()
            else:
                self.logar_erro(["("])
                self.sair()
        else:
            self.logar_erro(["if"])
        return False
        
    def CMD_SWITCH(self, cmd_switch_codigo):
        if self.tk_atual() == "switch":
            self.avanca_token()
            if self.tk_atual() == "(":
                self.avanca_token()
                expressao_codigo = ["",]
                if self.EXPRESSAO(expressao_codigo):
                    if self.tk_atual() == ")":
                        self.marca_token()
                        self.avanca_token()
                        cmd_bloco_case_codigo = ["",]
                        if self.CMD_BLOCO_CASE(expressao_codigo, cmd_bloco_case_codigo):
                            cmd_switch_codigo[0] = cmd_bloco_case_codigo[0]
                            return True
                        self.sair()
                    else:
                        self.logar_erro([")"])
                        self.sair()
                self.sair()
            else:
                self.logar_erro(["("])
                self.sair()
        else:
            self.logar_erro(["switch"])
        return False
 
    def CMD_BLOCO_CASE(self, cmd_bloco_case_expressao, cmd_bloco_case_codigo):
        if self.tk_atual() == "{":
            self.marca_token()
            self.avanca_token()
            cmd_case_codigo = ["",]
            if self.CMD_CASE(cmd_bloco_case_expressao, cmd_case_codigo):
                if self.tk_atual() == "}":
                    self.avanca_token()
                    cmd_bloco_case_codigo[0] = cmd_case_codigo[0]
                    return True
                else:
                    self.logar_erro(["}"])                    
        else:
            self.logar_erro(["{"])
        return False

    def CMD_CASE(self, cmd_case_expressao, cmd_case_codigo):
        if self.tk_atual() == "case":
            self.avanca_token()
            constante_valor = ["",]
            if self.CONSTANTE(constante_valor):
                if self.tk_atual() == ":":
                    self.avanca_token()
                    #cmd_codigo = ["",]
                    #if self.CMD(cmd_codigo)[0]:
                    lista_cmd_codigo = ["",]
                    if self.LISTA_CMD(lista_cmd_codigo):
                        cmd_case1_codigo = ["",]
                        if self.CMD_CASE(cmd_case_expressao, cmd_case1_codigo):
                            cmd_case_codigo[0] = "se " + cmd_case_expressao[0] + " = " + constante_valor[0] + " entao\n" + lista_cmd_codigo[0] + "\nsenao\n" + cmd_case1_codigo[0] + "\nfimse"
                            return True
                    self.sair()
                else:
                    self.logar_erro([":"])
                    self.sair()
            else:
                self.sair()
        else:
            self.logar_erro(["case"])
            cmd_default_codigo = ["",]
            if self.CMD_DEFAULT(cmd_default_codigo):
                cmd_case_codigo[0] = cmd_default_codigo[0]
                return True
        return False
 
    def CMD_DEFAULT(self, cmd_default_codigo):
        if self.tk_atual() == "default":
            self.avanca_token()
            if self.tk_atual() == ":":
                self.avanca_token()
                #cmd_codigo = ["",]
                #if self.CMD(cmd_codigo)[0]:
                lista_cmd_codigo = ["",]
                if self.LISTA_CMD(lista_cmd_codigo):
                    #cmd_default_codigo[0] = "\nsenao\n" + lista_cmd_codigo[0] #+ "\nfimse"
                    cmd_default_codigo[0] = lista_cmd_codigo[0]
                    return True
                self.sair()
            else:
                self.logar_erro([":"])
                self.sair()
        else:
            self.logar_erro(["default"])
        return False
 
    def CMD_BREAK(self, cmd_break_codigo):
        if self.tk_atual() == "break":
            self.avanca_token()
            cmd_break_codigo[0] = "interrompa"
            return True
        self.logar_erro(["break"])
        return False

    def CMD_RETURN(self, cmd_return_codigo):
        if self.tk_atual() == "return":
            self.avanca_token()
            expressao_codigo = ["",]
            if self.EXPRESSAO(expressao_codigo):
                cmd_return_codigo[0] = "retorne " + expressao_codigo[0]
                return True
            self.sair()
        self.logar_erro(["return"])
        return False

    def CMD_CONTINUE(self, cmd_continue_codigo):
        if self.tk_atual() == "continue":
            self.avanca_token()
            return True
        self.logar_erro(["continue"])
        return False

    def EXPRESSAO(self, expressao_codigo):
        atribuicao_codigo = ["",]
        if self.ATRIBUICAO(atribuicao_codigo):
            expressao_codigo[0] = atribuicao_codigo[0]
            return True
        return False
    
    def ATRIBUICAO(self, atribuicao_codigo):
        ou_logico_codigo = ["",]
        if self.OU_LOGICO(ou_logico_codigo):          
            op_atribuicao_codigo = ["",]
            id_val = ""
            if self.tk_anterior() == "id":
                id_val = self.analisador.token_anterior["lexema_real"]
            if self.OP_ATRIBUICAO(id_val, op_atribuicao_codigo):
                atribuicao1_codigo = [""]
                if self.ATRIBUICAO(atribuicao1_codigo):
                    atribuicao_codigo[0] = ou_logico_codigo[0] + op_atribuicao_codigo[0] + atribuicao1_codigo[0]
                    return True
                self.retroceder_token()
                return False
            atribuicao_codigo[0] = ou_logico_codigo[0]
            return True
        return False

    def OU_LOGICO(self, ou_logico_codigo):
        e_logico_codigo = ["",]
        if self.E_LOGICO(e_logico_codigo):
            ou_logicol_codh = [e_logico_codigo[0],]
            ou_logicol_cods = ["",]
            if self.OU_LOGICOL(ou_logicol_codh, ou_logicol_cods):
                ou_logico_codigo[0] = ou_logicol_cods[0]
                return True
        return False

    def OU_LOGICOL(self, ou_logicol_codh, ou_logicol_cods):
        if self.tk_atual() == "||":
            self.marca_token()
            self.avanca_token()
            e_logico_codigo = ["",]
            if self.E_LOGICO(e_logico_codigo):
                ou_logicol1_codh = [ou_logicol_codh[0] + " ou " + e_logico_codigo[0],]
                ou_logicol1_cods = ["",]
                if self.OU_LOGICOL(ou_logicol1_codh, ou_logicol1_cods):
                    ou_logicol_cods[0] = ou_logicol1_cods[0]
                    return True
                else:
                    return False
            else:
                self.retroceder_token()
                return False
        ou_logicol_cods[0] = ou_logicol_codh[0]
        return True

    def E_LOGICO(self, e_logico_codigo):
        igualdade_codigo = ["",]
        if self.IGUALDADE(igualdade_codigo):
            e_logicol_codh = [igualdade_codigo[0], ]
            e_logicol_cods = [""]
            if self.E_LOGICOL(e_logicol_codh, e_logicol_cods):
                e_logico_codigo[0] = e_logicol_cods[0]
                return True
        return False

    def E_LOGICOL(self, e_logicol_codh, e_logicol_cods):
        if self.tk_atual() == "&&":
            self.marca_token()
            self.avanca_token()
            igualdade_codigo = ["",]
            if self.IGUALDADE(igualdade_codigo):
                e_logicol1_codh = [e_logicol_codh[0] + " e " + igualdade_codigo[0],]
                e_logicol1_cods = ["",]
                if self.E_LOGICOL(e_logicol1_codh, e_logicol1_cods):
                    e_logicol_cods[0] = e_logicol1_cods[0]
                    return True
                else:
                    return False
            else:
                self.retroceder_token()
                return False
        e_logicol_cods[0] = e_logicol_codh[0]
        return True

    def OU_INCLUSIVO(self):
        if self.E_BITWISE():
            if self.OU_INCLUSIVOL():
                return True
        return False

    def OU_INCLUSIVOL(self):
        if self.tk_atual() == "|":
            self.marca_token()
            self.avanca_token()
            if self.E_BITWISE():
                if self.OU_INCLUSIVOL():
                    return True
                else:
                    return False
            else:
                self.retroceder_token()
                return False
        return True

    def E_BITWISE(self):
        if self.IGUALDADE():
            if self.E_BITWISEL():
                return True
        return False

    def E_BITWISEL(self):
        if self.tk_atual() == "&":
            self.marca_token()
            self.avanca_token()
            if self.IGUALDADE():
                if self.E_BITWISEL():
                    return True
                else:
                    return False
            else:
                self.retroceder_token()
                return False
        return True

    def IGUALDADE(self, igualdade_codigo):
        relacional_codigo = ["",]
        if self.RELACIONAL(relacional_codigo):
            igualdadel_codh = [relacional_codigo[0], ]
            igualdadel_cods = ["",]
            if self.IGUALDADEL(igualdadel_codh, igualdadel_cods):
                igualdade_codigo[0] = igualdadel_cods[0]
                return True
        return False

    def IGUALDADEL(self, igualdadel_codh, igualdadel_cods):
        op_igualdade_codigo = ["",]
        if self.OP_IGUALDADE(op_igualdade_codigo):
            relacional_codigo = ["",]
            if self.RELACIONAL(relacional_codigo):
                igualdadel1_codh = [igualdadel_codh[0] + op_igualdade_codigo[0] + relacional_codigo[0],]
                igualdadel1_cods = ["",]
                if self.IGUALDADEL(igualdadel1_codh, igualdadel1_cods):
                    igualdadel_cods[0] = igualdadel1_cods[0]
                    return True
                else:
                    return False
            else:
                self.retroceder_token()
                return False
        igualdadel_cods[0] = igualdadel_codh[0]
        return True

    def RELACIONAL(self, relacional_codigo):
        soma_subtracao_codigo = ["",]
        if self.SOMA_SUBTRACAO(soma_subtracao_codigo):
            relacionall_codh = [soma_subtracao_codigo[0],]
            relacionall_cods = ["",]
            if self.RELACIONALL(relacionall_codh, relacionall_cods):
                relacional_codigo[0] = relacionall_cods[0]
                return True
        return False

    def RELACIONALL(self, relacionall_codh, relacionall_cods):
        op_relacional_codigo = ["",]
        if self.OP_RELACIONAL(op_relacional_codigo):
            soma_subtracao_codigo = ["",]
            if self.SOMA_SUBTRACAO(soma_subtracao_codigo):
                relacionall1_codh = [relacionall_codh[0] + op_relacional_codigo[0] + soma_subtracao_codigo[0],]
                relacionall1_cods = ["",]
                if self.RELACIONALL(relacionall1_codh, relacionall1_cods):
                    relacionall_cods[0] = relacionall1_cods[0]
                    return True
                else:
                    return False
            else:
                self.retroceder_token()
                return False
        relacionall_cods[0] = relacionall_codh[0]
        return True

    def SHIFT_BIT(self):
        if self.SOMA_SUBTRACAO():
            if self.SHIFT_BITL():
                return True
        return False

    def SHIFT_BITL(self):
        if self.OP_SHIFT_BIT():
            if self.SOMA_SUBTRACAO():
                if self.SHIFT_BITL():
                    return True
                else:
                    return False
            else:
                self.retroceder_token()
                return False
        return True

    def SOMA_SUBTRACAO(self, soma_subtracao_codigo):
        multiplicacao_divisao_codigo = ["",]
        if self.MULTIPLICACAO_DIVISAO(multiplicacao_divisao_codigo):
            soma_subtracaol_codh = [multiplicacao_divisao_codigo[0],]
            soma_subtracaol_cods = ["",]
            if self.SOMA_SUBTRACAOL(soma_subtracaol_codh, soma_subtracaol_cods):
                soma_subtracao_codigo[0] = soma_subtracaol_cods[0]
                return True
        return False

    def SOMA_SUBTRACAOL(self, soma_subtracaol_codh, soma_subtracaol_cods):
        op_soma_subtracao_codigo = ["",]
        if self.OP_SOMA_SUBTRACAO(op_soma_subtracao_codigo):
            multiplicacao_divisao_codigo = ["",]
            if self.MULTIPLICACAO_DIVISAO(multiplicacao_divisao_codigo):
                soma_subtracaol1_codh = [soma_subtracaol_codh[0] + op_soma_subtracao_codigo[0] + multiplicacao_divisao_codigo[0],]
                soma_subtracaol1_cods = ["",]
                if self.SOMA_SUBTRACAOL(soma_subtracaol1_codh, soma_subtracaol1_cods):
                    soma_subtracaol_cods[0] = soma_subtracaol1_cods[0]
                    return True
                else:
                    return False
            else:
                self.retroceder_token()
                return False
        soma_subtracaol_cods[0] = soma_subtracaol_codh[0]
        return True

    def MULTIPLICACAO_DIVISAO(self, multiplicacao_divisao_codigo):
        negacao_codigo = ["",]
        if self.NEGACAO(negacao_codigo):
            multiplicacao_divisaol_codh = [negacao_codigo[0],]
            multiplicacao_divisaol_cods = ["",]
            if self.MULTIPLICACAO_DIVISAOL(multiplicacao_divisaol_codh, multiplicacao_divisaol_cods):
                multiplicacao_divisao_codigo[0] = multiplicacao_divisaol_cods[0]
                return True
        return False

    def MULTIPLICACAO_DIVISAOL(self, multiplicacao_divisaol_codh, multiplicacao_divisaol_cods):
        op_multiplicacao_divisao_codigo = ["",]
        if self.OP_MULTIPLICACAO_DIVISAO(op_multiplicacao_divisao_codigo):
            negacao_codigo = ["",]
            if self.NEGACAO(negacao_codigo):
                multiplicacao_divisaol1_codh = [multiplicacao_divisaol_codh[0] + op_multiplicacao_divisao_codigo[0] + negacao_codigo[0]]
                multiplicacao_divisaol1_cods = ["",]
                if self.MULTIPLICACAO_DIVISAOL(multiplicacao_divisaol1_codh, multiplicacao_divisaol1_cods):
                    multiplicacao_divisaol_cods[0] = multiplicacao_divisaol1_cods[0]
                    return True
                else:
                    return False
            else:
                self.retroceder_token()
                return False
        multiplicacao_divisaol_cods[0] = multiplicacao_divisaol_codh[0]
        return True

    def NEGACAO(self, negacao_codigo):
        if self.tk_atual() == "!":
            self.marca_token()
            self.avanca_token()
            negacao1_codigo = ["",]
            if self.NEGACAO(negacao1_codigo):
                negacao_codigo[0] = "nao " + negacao1_codigo[0]
                return True
            else:
                self.retroceder_token()
                return False
        else:
            unarios_codigo = ["",]
            if self.UNARIOS(unarios_codigo):
                negacao_codigo[0] = unarios_codigo[0]
                return True
            else:
                return False
        self.logar_erro(["!"])
        return False

    def UNARIOS(self, unarios_codigo):
        op_unarios_codigo = ["",]
        if self.OP_UNARIOS(op_unarios_codigo):
            pos_incremento_codigo = ["",]
            if self.POS_INCREMENTO(pos_incremento_codigo):
                unarios_codigo[0] = op_unarios_codigo[0] + pos_incremento_codigo[0]
                return True
            else:
                self.retroceder_token()
                return False
        else:
            pos_incremento_codigo = ["",]
            if self.POS_INCREMENTO(pos_incremento_codigo):
                unarios_codigo[0] = pos_incremento_codigo[0]
                return True
        return False

    def POS_INCREMENTO(self, pos_incremento_codigo):
        terminais_codigo = ["",]
        if self.TERMINAIS(terminais_codigo):
            pos_incrementol_codigo = ["",]
            id_val = ""
            if self.tk_anterior() == "id":
                id_val = self.analisador.token_anterior["lexema_real"]
            if self.POS_INCREMENTOL(pos_incrementol_codigo, id_val):
                pos_incremento_codigo[0] = terminais_codigo[0] + pos_incrementol_codigo[0]
                return True
        return False

    def POS_INCREMENTOL(self, pos_incrementol_codigo, id_val):
        op_pos_incremento_codigo = ["",]
        if self.OP_POS_INCREMENTO(op_pos_incremento_codigo, id_val):
            virgula_codigo = ["",]
            if self.VIRGULA(virgula_codigo):
                pos_incrementol_codigo[0] = op_pos_incremento_codigo[0] + virgula_codigo[0]
                return True
            else:
                self.retroceder_token()
                return False
        else:
            virgula_codigo = ["",]
            if self.VIRGULA(virgula_codigo):
                pos_incrementol_codigo[0] = virgula_codigo[0]
                return True
        return True

    def VIRGULA(self, virgula_codigo):
        if self.tk_atual() == ",":
            self.marca_token()
            self.avanca_token()
            expressao_codigo = ["",]
            if self.EXPRESSAO(expressao_codigo):
                virgula_codigo[0] = ", " + expressao_codigo[0]
                return True
            self.retroceder_token()
            return False
        virgula_codigo[0] = ""
        return True

    def TERMINAIS(self, terminais_codigo):
        constante_codigo = ["",]
        if self.CONSTANTE(constante_codigo):
            terminais_codigo[0] = constante_codigo[0]
            return True
        elif self.tk_atual() == "id":
            id_val = self.token_completo["lexema_real"]
            self.avanca_token()
            terminaisl_codigo = ["",]
            if self.TERMINAISL(terminaisl_codigo):
                terminais_codigo[0] = id_val + terminaisl_codigo[0]
                return True
        elif self.tk_atual() == "(":
            self.avanca_token()
            expressao_codigo = ["",]
            if self.EXPRESSAO(expressao_codigo):
                if self.tk_atual() == ")":
                    self.avanca_token()
                    terminais_codigo[0] = "(" + expressao_codigo[0] + ")"
                    return True
                self.logar_erro([")"])
        else:
            self.logar_erro(["id", "(", "cte_int", "cte_float", "cte_string"])
        return False

    def TERMINAISL(self, terminaisl_codigo):
        if self.tk_atual() == "(":
            self.avanca_token()
            expressao_codigo = ["",]
            if self.EXPRESSAO(expressao_codigo):
                if self.tk_atual() == ")":
                    self.avanca_token()
                    terminaisl_codigo[0] = "(" + expressao_codigo[0] + ")"
                    return True
                self.logar_erro([")"])
            return False
        terminaisl_codigo[0] = ""
        return True

    def OP_ATRIBUICAO(self, id_val, op_atribuicao_codigo):
        if self.tk_atual() in ["=", "+=", "-=", "*=", "/=", "%="]:
            if self.tk_atual() == "=":
                op_atribuicao_codigo[0] = " <- "
            elif self.tk_atual() == "+=":
                op_atribuicao_codigo[0] = " <- " + id_val + " + "
            elif self.tk_atual() == "-=":
                op_atribuicao_codigo[0] = " <- " + id_val + " - "
            elif self.tk_atual() == "*=":
                op_atribuicao_codigo[0] = " <- " + id_val + " * "
            elif self.tk_atual() == "/=":
                op_atribuicao_codigo[0] = " <- " + id_val + " / "
            elif self.tk_atual() == "%=":
                op_atribuicao_codigo[0] = " <- " + id_val + " % "
            self.marca_token()
            self.avanca_token()
            return True
        else:
            self.logar_erro(["=", "+=", "-=", "*=", "/=", "%=", ">>=", "<<=", "&=", "^=", "|="])
            return False

    def OP_IGUALDADE(self, op_igualdade_codigo):
        if self.tk_atual() in ["==", "!="]:
            if self.tk_atual() == "==":
                op_igualdade_codigo[0] = " = "
            else:
                op_igualdade_codigo[0] = " <> "
            self.marca_token()
            self.avanca_token()
            return True
        else:
            self.logar_erro(["==", "!="])
            return False

    def OP_RELACIONAL(self, op_relacional_codigo):
        if self.tk_atual() in ["<", ">", "<=", ">="]:
            op_relacional_codigo[0] = " " + self.tk_atual() + " "
            self.marca_token()
            self.avanca_token()
            return True
        else:
            self.logar_erro(["<", ">", "<=", ">="])
            return False

    def OP_SHIFT_BIT(self):
        if self.tk_atual() in [">>", "<<"]:
            self.marca_token()
            self.avanca_token()
            return True
        else:
            self.logar_erro([">>", "<<"])
            return False

    def OP_SOMA_SUBTRACAO(self, soma_subtracao_codigo):
        if self.tk_atual() in ["+", "-"]:
            soma_subtracao_codigo[0] = " " + self.tk_atual() + " "
            self.marca_token()
            self.avanca_token()
            return True
        else:
            self.logar_erro(["+", "-"])
            return False

    def OP_MULTIPLICACAO_DIVISAO(self, op_multiplicacao_divisao_codigo):
        if self.tk_atual() in ["*", "/", "%"]:
            op_multiplicacao_divisao_codigo[0] = " " + self.tk_atual() + " "
            self.marca_token()
            self.avanca_token()
            return True
        else:
            self.logar_erro(["*", "/", "%"])
            return False

    def OP_UNARIOS(self, op_unarios_codigo):
        if self.tk_atual() in ["++", "--", "-", "&"]:
            if self.tk_atual() == "&":
                op_unarios_codigo[0] = "&"
            self.marca_token()
            self.avanca_token()
            return True
        else:
            self.logar_erro(["++", "--", "-", "&"])
            return False

    def OP_POS_INCREMENTO(self, op_pos_incremento_codigo, id_val):
        if self.tk_atual() in ["++", "--"]:
            if self.tk_atual() == "++":
                op_pos_incremento_codigo[0] = " <- " + id_val + " + 1"
            else:
                op_pos_incremento_codigo[0] = " <- " + id_val + " - 1"
            self.marca_token()
            self.avanca_token()
            return True
        else:
            self.logar_erro(["++", "--"])
            return False

    def token_atual_completo(self):
        self.token_completo = self.analisador.token_atual

    def logar_erro(self, lista_esperados):
        esperados = ""
        for token in lista_esperados:
            if esperados:
                esperados += ", "
            esperados += token
        self.token_atual_completo()
        parametros = esperados, self.token_completo['lexema_real'], self.token_completo['linha'], self.token_completo['coluna']
        self.lista_erros.append((self.mensagem_erro) % parametros) 

    def sair(self):
        print self.lista_erros[-1]
        sys.exit(1)

    def tratar_printf_scanf(self, programa):
        linhas = programa.splitlines()
        linhas_printf = []
        linhas_scanf = []
        for linha in linhas:
            if "printf(" in linha: #testar se o printf nao esta entre aspas. Se estiver, eh string, e nao chamada de funcao
                linhas_printf.append(linha)
            elif "scanf(" in linha: #testar se o scanf nao esta entre aspas. Se estiver, eh string, e nao chamada de funcao
                linhas_scanf.append(linha)

        for linha in linhas_printf:
            nova_linha = ""
            string = linha[linha.find("\""):linha.rfind("\"") + 1]
            valores = linha[linha.find(string) + len(string):].replace(")","").split(",")
            string = string.split("\\n")
            valores.remove("")
            posicao_parametro = 0
            valor_atual = 0
            for trecho in string:
                inicio = 0
                string_escreva = ""
                if trecho and "%" not in trecho:
                    string_escreva = "\"" + trecho.strip("\"") + "\""                
                else:
                    while True:
                        posicao_parametro = trecho.find("%",inicio)
                        trecho_antes_parametro = trecho[inicio:posicao_parametro].strip("\"")
                        if trecho_antes_parametro:
                            if string_escreva:
                                string_escreva += ", "
                            string_escreva += "\"" + trecho_antes_parametro + "\""
                        if posicao_parametro != -1:
                            if string_escreva:
                                string_escreva += ", "
                            string_escreva += valores[valor_atual].strip()
                            valor_atual += 1
                            inicio =posicao_parametro + 2
                        else:
                            break

                if string_escreva:
                    if len(string) > 1 and string.index(trecho) < len(string) - 1:
                        string_escreva = "escreval(" + string_escreva + ")"
                    else:
                        string_escreva = "escreva(" + string_escreva + ")"
                    nova_linha = nova_linha + string_escreva + "\n"
            if nova_linha:
                programa = programa.replace(linha, nova_linha)
            
        for linha in linhas_scanf:
            nova_linha = "leia(%s)" % linha[linha.find(",")+1:linha.rfind(")")].replace("&","")
            programa = programa.replace(linha, nova_linha)

        return programa
