#-*- coding: utf-8 -*-
#
# Copyright (c) 2012 by Luciano Camargo Cruz <luciano@lccruz.net>, Joao Toss Molon <jtmolon@gmail.com>
#
# GNU General Public License (GPL)
#

import os
import sys
import string

class AnalisadorLexico(object):
    
    def __init__(self):
        self.arq_op_tokens = self.testaArquivo("docs/operadores.txt")
        self.arq_res_tokens = self.testaArquivo("docs/reservadas.txt")
        self.dic_id_tokens = {}
        self.dic_op_tokens = {}
        self.letras = string.ascii_letters + '_'
        self.numeros = string.digits
        self.dic_erro = {}
        self.populaTokens()
        self.linhas_fonte = []
        self.num_linha = 1
        self.num_coluna = 1
        self.token_atual = dict(id=0, lexema='', linha=0, coluna=0)
        self.token_anterior = dict(id=0, lexema='', linha=0, coluna=0)
        self.list_tokens_marcados = []

    def testaArquivo(self, arquivo_nome):
        """ Testa se o arquivo pode ser carregado
            recebe um arquivo_nome(string), retorna o mesmo se nao tiver erro
        """ 
        if not os.path.exists(arquivo_nome):
            print 'Erro no carregamento do arquivo %s' % (arquivo_nome)
            sys.exit()
        return arquivo_nome

    def printValuesTokens(self):
        """ Print as keys e values do dicionaio self.op_tokens
        """
        print "Operadores"
        for chave, valor in self.dic_op_tokens.items():
            print "%s : %s" % (chave, valor)

        print "\nToken IDs"
        for chave, valor in self.dic_id_tokens.items():
            print "%s : %s" % (chave, valor)


    def populaTokens(self):
        """ Efetua a populacao dos dicionarios self.dic_op_tokens, self.dic_id_tokens
        """
        #adiciona tokens dos operadores
        arquivo_op_tokens = open(self.arq_op_tokens, 'r')
        op_tokens = arquivo_op_tokens.readlines()
        id = 0
        for token in op_tokens:
            chave,valor = token.split('#')
            chave = chave.replace(' ','')
            chave = chave.replace('\n','')
            valor = valor.replace(' ','')
            valor = valor.replace('\n','')
            valor_temp = []
            if valor:
                valor_temp = valor.split('$')
            self.dic_op_tokens[chave] = valor_temp
            id += 1
            self.dic_id_tokens[chave] = id

        #adiciona manualmente porque o ^ nao eh um token!!!
        self.dic_op_tokens['^'] = ['=',]

        #adiciona tokens para id, constante int e constante float
        for chave in ('id', 'cte_int', 'cte_float', 'cte_string'):
            id += 1
            self.dic_id_tokens[chave] = id

        #adiciona tokens das palavras reservadas
        arquivo_res_tokens = open(self.arq_res_tokens, 'r')
        res_tokens = arquivo_res_tokens.readlines()
        for token in res_tokens:
            chave = token
            chave = chave.strip()
            id += 1
            self.dic_id_tokens[chave] = id


    def analisaArquivo(self, nome_arquivo):
        """ Efetua a analise lexica de um arquivo fonte
        """
        nome_arquivo = self.testaArquivo(nome_arquivo)
        #abre o arquivo de entrada
        arquivo = open(nome_arquivo, 'r')
        self.linhas_fonte = arquivo.readlines()
        return True

    def leToken(self):
        """ Le o proximo token
        """
        #coluna do arquivo onde comeca o reconhecimento do token 
        num_coluna = self.num_coluna
        pos = self.num_coluna - 1
        #numero da linha onde começa o reconhecimento do token
        num_linha = self.num_linha
        #conteudo da linha do arquivo
        token = ''
        token_completo = {}

        #iteracao pelas linhas do arquivo
        while self.num_linha <= len(self.linhas_fonte):
            linha = self.linhas_fonte[self.num_linha - 1]

            #iteracao pelas colunas da linha
            while pos < len(linha):
                if not token: #se token esta vazio, tenho que descobrir qual o caso
                    #se o caracter atual for espaco em branco, ingora e segue reconhecendo
                    if linha[pos] == ' ':
                        pos += 1
                        num_coluna += 1
                        continue
                    #se o caracter for fim de linha, avanca a linha e zera o pos
                    if linha[pos] == '\n':
                        self.num_linha += 1
                        num_linha += 1
                        pos = 0
                        num_coluna = 1
                        break
                    #se comeca por letra, entro no caso das letras
                    elif linha[pos] in self.letras:
                        caso = "letras"
                    #se comeca por numero, entro no caso dos inteiros
                    elif linha[pos] in self.numeros:
                        caso = "inteiros"
                    elif linha[pos] == "\"":
                        caso = "string"
                    else:
                    #se nao for nenhum dos dos anteriores, e um operador
                        caso = "operadores"
                
                token = token + linha[pos]

                if caso == "letras":
                    #reconheco as letras
                    if pos + 1 < len(linha):
                        #se o proximo caracter for uma letra, _ ou numero, continuo reconhecendo letras
                        if linha[pos + 1] in (self.letras + self.numeros):
                            pos += 1
                            continue
                elif caso in ("inteiros", "float"):
                    #reconheco os numeros
                    if pos + 1 < len(linha):
                        #se for um numero, continuo reconhecendo
                        if linha[pos + 1] in self.numeros:
                            pos += 1
                            continue
                        #se for um . mudo o caso para floar e continuo reconhecendo
                        if linha[pos +1] == '.' and caso == "inteiros":
                            pos += 1
                            caso = "float"
                            continue
                elif caso == "string":
                    #reconheco o conteudo da string
                    if pos + 1 < len(linha):
                        pos += 1
                        if linha[pos] == "\"":
                            token = token + linha[pos]
                        else:
                            continue                    
                else:
                    #reconheco os operadores
                    transicoes = self.dic_op_tokens[token]
                    if transicoes and (pos + 1) < len(linha):
                        if linha[pos + 1] in transicoes:
                            pos += 1
                            continue              

                lexema = token
                lexema_de_fato = lexema
                if caso == 'letras':
                    # testa se nao eh reservada
                    if token not in self.dic_id_tokens.keys():
                        token = 'id'
                        lexema = token
                elif caso == "inteiros":
                    token = 'cte_int'
                    lexema = token
                elif caso == "float":
                    token = 'cte_float'
                    lexema = token
                elif caso == "string":
                    token = 'cte_string'
                    lexema = token
                

                if token not in self.dic_id_tokens.keys():
                    self.dic_erro = dict(linha=num_linha, coluna=num_coluna, caracter=linha[num_coluna-1])
                    return False 

                #adiciona token ao retorno da funcao
                token_completo = dict(id=self.dic_id_tokens[token], lexema=lexema, linha=num_linha, coluna=num_coluna, lexema_real=lexema_de_fato)
                self.num_coluna = pos + 2
                self.token_anterior = self.token_atual
                self.token_atual = token_completo
            
                #enquanto tiver algum token para ler, retorna True
                return True

        #se nao tiver mais nada para ler, retorna False
        token_completo = dict(id=-1, lexema="Fim de Arquivo", linha=num_linha, coluna=num_coluna, lexema_real="Fim de Arquivo")
        self.token_atual = token_completo

        return False

    def marcarToken(self):
        """Marca qual o token atual
        """
        #empilho o token_atual
        self.list_tokens_marcados.append((self.token_atual, self.token_anterior))

    def retrocederToken(self):
        """Retrocede um token
        """
        #se tenho algum token marcado
        if self.list_tokens_marcados:
            #desempilho o ultimo token marcado
            token = self.list_tokens_marcados.pop()
            self.token_atual = token[0]
            self.token_anterior = token[1]
            self.num_coluna = self.token_atual['coluna'] + len(self.token_atual['lexema'])
            self.num_linha = self.token_atual['linha']
