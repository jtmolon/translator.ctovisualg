# -*- coding: utf-8 -*-

from le_token import AnalisadorLexico
import sys

class AnalisadorSintatico(object):
     
    def __init__(self, nome_arquivo):
        self.analisador = AnalisadorLexico()
        self.analisador.analisaArquivo(nome_arquivo)
        self.lista_erros = []
        self.blocos = []
        self.token_completo = {}
        self.mensagem_erro = "Erro: esperado %s. Encontrado: %s. Linha: %s Coluna: %s"
        self.avanca_token()

    def analise_sintatica(self):
        if self.PROGRAMA():
            print "Ok"
        else:
            print "Nao Ok"
            for item in self.lista_erros:
                print item

    def tk_atual(self):
        return self.analisador.token_atual['lexema']
    
    def tk_anterior(self):
        return self.analisador.token_anterior['lexema']

    def avanca_token(self):
        self.analisador.leToken()

    def marca_token(self):
        self.analisador.marcarToken()

    def retroceder_token(self):
        self.analisador.retrocederToken()

    def PROGRAMA(self):
        if self.LISTA_DECLARACOES():
            if self.LISTA_FUNCOES():
                if self.tk_atual() == "Fim de Arquivo":
                    return True
                else:
                    self.logar_erro(["Fim de Arquivo"])
                    self.sair()
            return False
        return False

    def LISTA_DECLARACOES(self):
        if self.DEC_VAR_FORA():
            if self.tk_atual() == ";":
                self.avanca_token()
                if self.LISTA_DECLARACOES():
                    return True
            else:
                self.logar_erro([";"])
                self.sair()
                return False
        return True

    def LISTA_FUNCOES(self):
        if self.DEC_FUN():
            if self.LISTA_DECLARACOES():
                if self.LISTA_FUNCOES():
                    return True
        else:
            return False
        return True

    def DEC_VAR_FORA(self):
        tipo, tokens_tipo = self.TIPO()
        if tipo:
            if self.tk_atual() == 'id':
                self.marca_token()
                self.avanca_token()
                if self.RESTO_LIDS():
                    if self.tk_atual() == ';':
                        return True
                self.retroceder_token()
        for i in range(0, tokens_tipo):
            self.retroceder_token()
        return False

    def CMD_DEC_VAR(self):
        tipo, tokens_tipo = self.TIPO()
        if tipo:
            if self.tk_atual() == 'id':
                self.marca_token()
                self.avanca_token()
                if self.RESTO_LIDS():
                    return True
                self.retroceder_token()
        for i in range(0, tokens_tipo):
            self.retroceder_token()
        return False

    def TIPO(self):
        tokens_avancados = 0
        tipo_mod1, avan_mod1 = self.TIPO_MOD1()
        if tipo_mod1:
            tokens_avancados += avan_mod1
            tipo_mod2, avan_mod2 = self.TIPO_MOD2()
            if tipo_mod2:
                tokens_avancados += avan_mod2
                if self.TIPO_BASE():
                    tokens_avancados += 1
                    return True, tokens_avancados
        return False, tokens_avancados

    def TIPO_MOD1(self):
        tokens_avancados = 0
        if self.tk_atual() in ['unsigned','signed']:
            self.marca_token()
            self.avanca_token()
            tokens_avancados = 1
        return True, tokens_avancados

    def TIPO_MOD2(self):
        tokens_avancados = 0
        if self.tk_atual() in ['long','short']:
            self.marca_token()
            self.avanca_token()
            tokens_avancados = 1
        return True, tokens_avancados

    def TIPO_BASE(self):
        if self.tk_atual() in ['int','float','char','double','void']:
            self.marca_token()
            self.avanca_token()
            return True
        else:
            self.logar_erro(['int','float','char','double','void'])
            return False

    def RESTO_LIDS(self):
        if self.tk_atual() == ",":
            self.marca_token()
            self.avanca_token()
            if self.tk_atual() == "id":
                self.marca_token()
                self.avanca_token()
                if self.RESTO_LIDS():
                    return True
                else:
                    self.retroceder_token()
                    return False
            else:
                self.retroceder_token()
                return False
        elif self.tk_atual() == "=":
            self.marca_token()
            self.avanca_token()
            if self.FINAIS():
                if self.RESTO_LIDS():
                    return True
            else:
                self.retroceder_token()
                return False
        elif self.tk_atual() == "id":
            self.avanca_token()
            return True
        elif self.tk_atual() == ";":
            return True
        else:
            self.logar_erro([',', '=', 'id', ';'])
        return True

    def FINAIS(self):
        if self.tk_atual() == "id":
            self.avanca_token()
            return True
        elif self.CONSTANTE():
            return True
        self.logar_erro(['id', 'cte_int', 'cte_float'])
        return False

    def CONSTANTE(self):
        if self.tk_atual() in ['cte_int','cte_float','cte_string']:
            self.avanca_token()
            return True
        else:
            self.logar_erro(['cte_int', 'cte_float','cte_string'])
            return False

    def DEC_FUN(self):
        tipo_opt, tokens_avancados = self.TIPO_OPT()
        if tipo_opt:
            if self.tk_atual() == 'id':
                self.marca_token()
                self.avanca_token()
                if self.tk_atual() == '(':
                    self.marca_token()
                    self.avanca_token()
                    if self.LISTA_ARGS():
                        if self.tk_atual() == ')':  
                            self.avanca_token()
                            if self.BLOCO_CMD_FUN():
                                return True
                        else:
                            self.logar_erro([")"])
                            self.sair()
                    self.sair()
                else:
                    self.logar_erro(["(", ";"])
                    self.sair()
            else:
                self.logar_erro(['id'])
            for i in range(0, tokens_avancados):
                self.retroceder_token()
        return False

    def TIPO_OPT(self):
        tipo, tokens_avancados = self.TIPO()
        if tipo:
            return True, tokens_avancados
        return True, tokens_avancados

    def LISTA_ARGS(self):
        if self.ARG():
            if self.RESTO_LARGS():
                return True
        return False
 
    def ARG(self):
        tipo, tokens_avancados = self.TIPO()
        if tipo:
            if self.tk_atual() == "id":
                self.avanca_token()
                return True
            else:
                self.logar_erro(['id'])
        return False

    def RESTO_LARGS(self):
        if self.tk_atual() == ",":
            self.avanca_token()
            if self.ARG():
                if self.RESTO_LARGS():
                    return True
                else:
                    return False
            else:
                return False
        return True

    def BLOCO_CMD_FUN(self):
        if self.tk_atual() == "{":
            self.marca_token()
            self.avanca_token()
            if self.LISTA_CMD():
                if self.tk_atual() == "}":
                    self.avanca_token()
                    return True
                self.logar_erro(["}"])
                self.sair()
                return False
            self.sair()
        else:
            self.logar_erro(["{"])
        return False

    def CMD_BLOCO_CMD(self):
        if self.tk_atual() == "{":
            self.marca_token()
            self.avanca_token()
            self.blocos.append(1)
            if self.LISTA_CMD():
                if self.tk_atual() == "}":
                    if self.tk_anterior() in [";", "{", "}"]:
                        return True
                else:
                    if self.tk_atual() == "Fim de Arquivo": 
                        if self.tk_anterior() in [";", "}"]:
                            return True
                        self.logar_erro(["}"])
                        self.sair()
                self.logar_erro(["}"])
                self.sair()
                return False
            self.sair()
        else:
            self.logar_erro(["{"])
        return False

    def LISTA_CMD(self):
        cmd, is_bloco_cmd = self.CMD() 
        if cmd:
            if self.tk_atual() in ["}", ";"]:
                if self.tk_atual() == ";": 
                    self.avanca_token()
                elif is_bloco_cmd and self.tk_anterior() in [";", "{", "}"]:
                    if self.blocos:
                        self.blocos.pop()
                        self.avanca_token()
                else:
                    self.logar_erro([";"])
                    return False
                if self.LISTA_CMD():
                    return True
            if self.tk_anterior() in ["}", ";"]:
                if self.LISTA_CMD():
                    return True
            self.logar_erro([";"])
            self.sair()
            return False
        return True

    def CMD(self):
        if self.CMD_WHILE():
            return True, self.tk_atual() == "}"
        elif self.CMD_DOWHILE():
            return True, self.tk_atual() == "}"
        elif self.CMD_FOR():
            return True, self.tk_atual() == "}"
        elif self.CMD_IF():
            return True, self.tk_atual() == "}"
        elif self.CMD_SWITCH():
            return True, self.tk_atual() == "}"
        elif self.CMD_CASE():
            return True, self.tk_atual() == "}"
        elif self.CMD_DEFAULT():
            return True, self.tk_atual() == "}"
        elif self.CMD_DEC_VAR():
            return True, self.tk_atual() == "}"
        elif self.CMD_ATRIBUICAO():
            return True, self.tk_atual() == "}"
        elif self.CMD_BLOCO_CMD():
            return True, self.tk_atual() == "}"
        elif self.CMD_BREAK():
            return True, self.tk_atual() == "}"
        elif self.CMD_CONTINUE():
            return True, self.tk_atual() == "}"
        elif self.CMD_RETURN():
            return True, self.tk_atual() == "}"
        elif self.tk_atual() == ";":
            return True, self.tk_atual() == "}"
        else:
            if self.tk_atual() == "Fim de Arquivo":
                self.logar_erro(["}"])
            else:
                self.logar_erro([";"])
        return False, False

    def CMD_ATRIBUICAO(self):
        if self.tk_atual() == "id":
            self.avanca_token()
            if self.CMD_ATRIBUICAOL():
                return True
            return True
        elif self.OP_POS_INCREMENTO():
            self.marca_token()
            self.avanca_token()
            if self.tk_atual() == "id":
                return True
            self.logar_erro(["id"])
            self.sair()
        return False

    def CMD_ATRIBUICAOL(self):
        if self.OP_ATRIBUICAO():
            if self.EXPRESSAO():
                return True
            return False
        elif self.OP_POS_INCREMENTO():
            return True
        elif self.tk_atual() == "(":
            self.marca_token()
            self.avanca_token()
            if self.EXPRESSAO():
                if self.tk_atual() == ")":
                    self.avanca_token()
                    return True
                self.logar_erro([")"])
                self.sair()
            self.sair()
        self.logar_erro(["("])
        return False
            
    def CMD_WHILE(self):
        if self.tk_atual() == "while":
            self.marca_token()
            self.avanca_token()
            if self.tk_atual() == "(":
                self.marca_token()
                self.avanca_token()
                if self.EXPRESSAO():
                    if self.tk_atual() == ")":
                        self.marca_token()
                        self.avanca_token()                        
                        if self.CMD()[0]:
                            return True
                        self.sair()
                    else:
                        self.logar_erro([")"])
                        self.sair()
                self.sair()
            else:
                self.logar_erro(["("])
                self.sair()
            self.retroceder_token()
        else: 
            self.logar_erro(["while"])
        return False
            
    def CMD_DOWHILE(self):
        if self.tk_atual() == "do":
            self.avanca_token()
            if self.CMD()[0]:
                self.avanca_token()
                if self.tk_atual() == "while":
                    self.avanca_token()
                    if self.tk_atual() == "(":
                        self.avanca_token()
                        if self.EXPRESSAO():
                            if self.tk_atual() == ")":
                                self.avanca_token()
                                if self.tk_atual() == ";":
                                    return True
                                else:
                                    self.logar_erro([";"])
                                    self.sair()
                            else:
                                self.logar_erro([")"])
                                self.sair()
                        self.sair()
                    else:
                        self.logar_erro(["("])
                        self.sair()
                else:
                    self.logar_erro(["while"])
                    self.sair()
            self.sair()
        else:
            self.logar_erro(["do"])
        return False
 
    def CMD_FOR(self):
        if self.tk_atual() == "for":
            self.avanca_token()
            if self.tk_atual() == "(":
                self.avanca_token()
                if self.LISTA_INI():
                    if self.tk_atual() == ";":
                        self.avanca_token()
                        if self.EXPRESSAO_OPT():
                            if self.tk_atual() == ";":
                                self.avanca_token()
                                if self.LISTA_INC():
                                    if self.tk_atual() == ")":
                                        self.avanca_token()
                                        if self.CMD()[0]:
                                            return True
                                        self.sair()
                                    else:
                                        self.logar_erro([")"])
                                        self.sair()
                                else:
                                    self.sair()
                            else:
                                self.logar_erro([";"])
                                self.sair()
                        self.sair()
                    else:
                        self.logar_erro([";"])
                        self.sair()
                else:
                    self.sair()
            else:
                self.logar_erro(["("])
                self.sair()
        else:
            self.logar_erro(["for"])
        return False
   
    def LISTA_INI(self):
        tipo_opt, tokens_avancados = self.TIPO_OPT()
        if tipo_opt:
            if self.LISTA_IDS_INI():
                return True
        #se estou com ; e porque achei uma LISTA_INI ou porque nao tinha nada
        if self.tk_atual() == ";":
            return True
        else:
            self.logar_erro([";"])
            self.sair()
        return False

    def LISTA_IDS_INI(self):
        if self.tk_atual() == "id":
            self.avanca_token()
            if self.RESTO_LIDS_INI():
                return True
            self.sair()
        else:
            self.logar_erro(["id"])
        return False

    def RESTO_LIDS_INI(self):
        if self.tk_atual() == ",":
            self.avanca_token()
            if self.tk_atual() == "id":
                self.marca_token()
                self.avanca_token()
                if self.RESTO_LIDS_INI():
                    return True
                self.retroceder_token()
            self.logar_erro(["id"])
            self.sair()
        elif self.tk_atual() == "=":
            self.avanca_token()
            if self.FINAIS():
                if self.RESTO_LIDS_INI():
                    return True
            self.sair()
        else:
            self.logar_erro([",", "="])
        return True

    def EXPRESSAO_OPT(self):
        if self.EXPRESSAO():
            return True
        return True

    def LISTA_INC(self):
        if self.EXPRESSAO():
            return True
        return True

    def CMD_IF(self):
        if self.tk_atual() == "if":
            self.avanca_token()
            if self.tk_atual() == "(":
                self.marca_token()
                self.avanca_token()
                if self.EXPRESSAO():
                    if self.tk_atual() == ")":
                        self.avanca_token()
                        if self.CMD()[0]:                            
                            self.marca_token()
                            self.avanca_token()
                            if self.tk_atual() == "else":
                                self.avanca_token()
                                if self.CMD()[0]:
                                    return True
                                self.sair()
                            else:
                                self.retroceder_token()
                                return True                                
                        else:
                            self.sair()
                    else:
                        self.logar_erro([")"])
                        self.sair()
                else:
                    self.sair()
            else:
                self.logar_erro(["("])
                self.sair()
        else:
            self.logar_erro(["if"])
        return False
        
    def CMD_SWITCH(self):
        if self.tk_atual() == "switch":
            self.avanca_token()
            if self.tk_atual() == "(":
                self.avanca_token()
                if self.EXPRESSAO():
                    if self.tk_atual() == ")":
                        self.marca_token()
                        self.avanca_token()
                        if self.CMD()[0]:
                            return True
                        self.sair()
                    else:
                        self.logar_erro([")"])
                        self.sair()
                self.sair()
            else:
                self.logar_erro(["("])
                self.sair()
        else:
            self.logar_erro(["switch"])
        return False
 
    def CMD_CASE(self):
        if self.tk_atual() == "case":
            self.avanca_token()
            if self.CONSTANTE():
                if self.tk_atual() == ":":
                    self.avanca_token()
                    if self.CMD()[0]:
                        return True
                    self.sair()
                else:
                    self.logar_erro([":"])
                    self.sair()
            else:
                self.sair()
        else:
            self.logar_erro(["case"])
        return False
 
    def CMD_DEFAULT(self):
        if self.tk_atual() == "default":
            self.avanca_token()
            if self.tk_atual() == ":":
                self.avanca_token()
                if self.CMD()[0]:
                    return True
                self.sair()
            else:
                self.logar_erro([":"])
                self.sair()
        else:
            self.logar_erro(["default"])
        return False
 
    def CMD_BREAK(self):
        if self.tk_atual() == "break":
            self.avanca_token()
            return True
        self.logar_erro(["break"])
        return False

    def CMD_RETURN(self):
        if self.tk_atual() == "return":
            self.avanca_token()
            if self.EXPRESSAO():
                return True
            self.sair()
        self.logar_erro(["return"])
        return False

    def CMD_CONTINUE(self):
        if self.tk_atual() == "continue":
            self.avanca_token()
            return True
        self.logar_erro(["continue"])
        return False

    def EXPRESSAO(self):
        if self.ATRIBUICAO():
            return True
        return False
    
    def ATRIBUICAO(self):
        if self.OU_LOGICO():
            if self.OP_ATRIBUICAO():
                if self.ATRIBUICAO():
                    return True
                self.retroceder_token()
                return False
            return True
        return False

    def OU_LOGICO(self):
        if self.E_LOGICO():
            if self.OU_LOGICOL():
                return True
        return False

    def OU_LOGICOL(self):
        if self.tk_atual() == "||":
            self.marca_token()
            self.avanca_token()
            if self.E_LOGICO():
                if self.OU_LOGICOL():
                    return True
                else:
                    return False
            else:
                self.retroceder_token()
                return False
        return True

    def E_LOGICO(self):
        if self.OU_INCLUSIVO():
            if self.E_LOGICOL():
                return True
        return False

    def E_LOGICOL(self):
        if self.tk_atual() == "&&":
            self.marca_token()
            self.avanca_token()
            if self.OU_INCLUSIVO():
                if self.E_LOGICOL():
                    return True
                else:
                    return False
            else:
                self.retroceder_token()
                return False
        return True

    def OU_INCLUSIVO(self):
        if self.E_BITWISE():
            if self.OU_INCLUSIVOL():
                return True
        return False

    def OU_INCLUSIVOL(self):
        if self.tk_atual() == "|":
            self.marca_token()
            self.avanca_token()
            if self.E_BITWISE():
                if self.OU_INCLUSIVOL():
                    return True
                else:
                    return False
            else:
                self.retroceder_token()
                return False
        return True

    def E_BITWISE(self):
        if self.IGUALDADE():
            if self.E_BITWISEL():
                return True
        return False

    def E_BITWISEL(self):
        if self.tk_atual() == "&":
            self.marca_token()
            self.avanca_token()
            if self.IGUALDADE():
                if self.E_BITWISEL():
                    return True
                else:
                    return False
            else:
                self.retroceder_token()
                return False
        return True

    def IGUALDADE(self):
        if self.RELACIONAL():
            if self.IGUALDADEL():
                return True
        return False

    def IGUALDADEL(self):
        if self.OP_IGUALDADE():
            if self.RELACIONAL():
                if self.IGUALDADEL():
                    return True
                else:
                    return False
            else:
                self.retroceder_token()
                return False
        return True

    def RELACIONAL(self):
        if self.SHIFT_BIT():
            if self.RELACIONALL():
                return True
        return False

    def RELACIONALL(self):
        if self.OP_RELACIONAL():
            if self.SHIFT_BIT():
                if self.RELACIONALL():
                    return True
                else:
                    return False
            else:
                self.retroceder_token()
                return False
        return True

    def SHIFT_BIT(self):
        if self.SOMA_SUBTRACAO():
            if self.SHIFT_BITL():
                return True
        return False

    def SHIFT_BITL(self):
        if self.OP_SHIFT_BIT():
            if self.SOMA_SUBTRACAO():
                if self.SHIFT_BITL():
                    return True
                else:
                    return False
            else:
                self.retroceder_token()
                return False
        return True

    def SOMA_SUBTRACAO(self):
        if self.MULTIPLICACAO_DIVISAO():
            if self.SOMA_SUBTRACAOL():
                return True
        return False

    def SOMA_SUBTRACAOL(self):
        if self.OP_SOMA_SUBTRACAO():
            if self.MULTIPLICACAO_DIVISAO():
                if self.MULTIPLICACAO_DIVISAOL():
                    return True
                else:
                    return False
            else:
                self.retroceder_token()
                return False
        return True

    def MULTIPLICACAO_DIVISAO(self):
        if self.NEGACAO():
            if self.MULTIPLICACAO_DIVISAOL():
                return True
        return False

    def MULTIPLICACAO_DIVISAOL(self):
        if self.OP_MULTIPLICACAO_DIVISAO():
            if self.NEGACAO():
                if self.MULTIPLICACAO_DIVISAOL():
                    return True
                else:
                    return False
            else:
                self.retroceder_token()
                return False
        return True

    def NEGACAO(self):
        if self.tk_atual() == "!":
            self.marca_token()
            self.avanca_token()
            if self.NEGACAO():
                return True
            else:
                self.retroceder_token()
                return False
        else:
            if self.UNARIOS():
                return True
            else:
                return False
        self.logar_erro(["!"])
        return False

    def UNARIOS(self):
        if self.OP_UNARIOS():
            if self.POS_INCREMENTO():
                return True
            else:
                self.retroceder_token()
                return False
        elif self.POS_INCREMENTO():
            return True
        return False

    def POS_INCREMENTO(self):
        if self.TERMINAIS():
            if self.POS_INCREMENTOL():
                return True
        return False

    def POS_INCREMENTOL(self):
        if self.OP_POS_INCREMENTO():
            if self.VIRGULA():
                return True
            else:
                self.retroceder_token()
                return False
        elif self.VIRGULA():
            return True
        return True

    def VIRGULA(self):
        if self.tk_atual() == ",":
            self.marca_token()
            self.avanca_token()
            if self.EXPRESSAO():
                return True
            self.retroceder_token()
            return False
        return True

    def TERMINAIS(self):
        if self.CONSTANTE():
            return True
        elif self.tk_atual() == "id":
            self.avanca_token()
            if self.TERMINAISL():
                return True
        elif self.tk_atual() == "(":
            self.avanca_token()
            if self.EXPRESSAO():
                if self.tk_atual() == ")":
                    self.avanca_token()
                    return True
                self.logar_erro([")"])
        else:
            self.logar_erro(["id", "(", "cte_int", "cte_float"])
        return False

    def TERMINAISL(self):
        if self.tk_atual() == "(":
            self.avanca_token()
            if self.EXPRESSAO():
                if self.tk_atual() == ")":
                    self.avanca_token()
                    return True
                self.logar_erro([")"])
            return False
        return True

    def OP_ATRIBUICAO(self):
        if self.tk_atual() in ["=", "+=", "-=", "*=", "/=", "%=", ">>=", "<<=", "&=", "^=", "|="]:
            self.marca_token()
            self.avanca_token()
            return True
        else:
            self.logar_erro(["=", "+=", "-=", "*=", "/=", "%=", ">>=", "<<=", "&=", "^=", "|="])
            return False

    def OP_IGUALDADE(self):
        if self.tk_atual() in ["==", "!="]:
            self.marca_token()
            self.avanca_token()
            return True
        else:
            self.logar_erro(["==", "!="])
            return False

    def OP_RELACIONAL(self):
        if self.tk_atual() in ["<", ">", "<=", ">="]:
            self.marca_token()
            self.avanca_token()
            return True
        else:
            self.logar_erro(["<", ">", "<=", ">="])
            return False

    def OP_SHIFT_BIT(self):
        if self.tk_atual() in [">>", "<<"]:
            self.marca_token()
            self.avanca_token()
            return True
        else:
            self.logar_erro([">>", "<<"])
            return False

    def OP_SOMA_SUBTRACAO(self):
        if self.tk_atual() in ["+", "-"]:
            self.marca_token()
            self.avanca_token()
            return True
        else:
            self.logar_erro(["+", "-"])
            return False

    def OP_MULTIPLICACAO_DIVISAO(self):
        if self.tk_atual() in ["*", "/", "%"]:
            self.marca_token()
            self.avanca_token()
            return True
        else:
            self.logar_erro(["*", "/", "%"])
            return False

    def OP_UNARIOS(self):
        if self.tk_atual() in ["++", "--", "-", "&"]:
            self.marca_token()
            self.avanca_token()
            return True
        else:
            self.logar_erro(["++", "--", "-", "&"])
            return False

    def OP_POS_INCREMENTO(self):
        if self.tk_atual() in ["++", "--"]:
            self.marca_token()
            self.avanca_token()
            return True
        else:
            self.logar_erro(["++", "--"])
            return False

    def token_atual_completo(self):
        self.token_completo = self.analisador.token_atual

    def logar_erro(self, lista_esperados):
        esperados = ""
        for token in lista_esperados:
            if esperados:
                esperados += ", "
            esperados += token
        self.token_atual_completo()
        parametros = esperados, self.token_completo['lexema_real'], self.token_completo['linha'], self.token_completo['coluna']
        self.lista_erros.append((self.mensagem_erro) % parametros) 

    def sair(self):
        print self.lista_erros[-1]
        sys.exit(1)
