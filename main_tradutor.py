# -*- coding: utf-8 -*-
#
# Copyright (c) 2012 by Luciano Camargo Cruz <luciano@lccruz.net>, Joao Toss Molon <jtmolon@gmail.com>
#
# GNU General Public License (GPL)
#
from tradutor import AnalisadorSintatico

nome_arquivo = raw_input("Digite o nome do arquivo que será analisado: ")
#nome_arquivo = "tests/fonte.c"
analisador = AnalisadorSintatico(nome_arquivo)
analisador.analise_sintatica()
