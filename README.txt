Analisador Léxico desenvolvido em Python.

Contributors:
    - Luciano Camargo Cruz    <luciano@lccruz.net>
    - Joao Toss Molon         <jtmolon@gmail.com>

License: 
    GPL 

Requisitos:
    Python >= 2.6

Rodar App:
    Tradutor:
        No terminal, executar: $python main_tradutor.py        
        Informar o caminho do arquivo C a ser traduzido
